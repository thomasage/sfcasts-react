<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;

final class ApiCsrfValidationSubscriber implements EventSubscriberInterface
{
    /**
     * @return array<string,string>
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'kernel.request' => 'onKernelRequest',
        ];
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        if (!$event->isMasterRequest()) {
            return;
        }
        $request = $event->getRequest();
        if ($request->isMethodSafe()) {
            return;
        }
        if (!$request->attributes->get('_is_api')) {
            return;
        }
        if ('application/json' !== $request->headers->get('Content-Type')) {
            $response = new JsonResponse(['message' => 'Invalid Content-Type'], Response::HTTP_UNSUPPORTED_MEDIA_TYPE);
            $event->setResponse($response);
        }
    }
}
