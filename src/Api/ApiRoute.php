<?php

declare(strict_types=1);

namespace App\Api;

use Symfony\Component\Routing\Annotation\Route;

/**
 * @Annotation
 */
final class ApiRoute extends Route
{
    /**
     * @return array<mixed>
     */
    public function getDefaults(): array
    {
        return array_merge(
            ['_is_api' => true],
            parent::getDefaults()
        );
    }
}
