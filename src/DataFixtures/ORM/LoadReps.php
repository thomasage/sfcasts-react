<?php

namespace App\DataFixtures\ORM;

use App\Entity\RepLog;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadReps extends Fixture
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $items = array_flip(RepLog::getThingsYouCanLiftChoices());

        $names = [
            ['Brad', 'Kitt'],
            ['Cat', 'Middleton'],
            ['Cindy', 'Clawford'],
            ['Diane', 'Kitten'],
            ['Fuzz', 'Aldrin'],
            ['Hunter S.', 'Thomcat'],
            ['J.R.R', 'Tollkitten'],
            ['Madelion', 'Albright'],
            ['Meowly', 'Cyrus'],
            ['Ron', 'Furgandy'],
        ];

        foreach ($names as $name) {
            $firstName = $name[0];
            $lastName = $name[1];

            $user = new User();
            $username = sprintf('%s_%s', $firstName, $lastName);
            $username = strtolower($username);
            $username = str_replace(' ', '', $username);
            $username = str_replace('.', '', $username);
            $user->setUsername($username);
            $user->setEmail($user->getUsername().'@example.com');
            $user->setPlainPassword('pumpup');
            $user->setFirstName($firstName);
            $user->setLastName($lastName);
            $user->setEnabled(true);
            $manager->persist($user);

            for ($j = 0; $j < rand(1, 5); ++$j) {
                $repLog = new RepLog();
                $repLog->setUser($user);
                $repLog->setReps(rand(1, 30));
                $repLog->setItem(array_rand($items));
                $manager->persist($repLog);
            }
        }

        $manager->flush();
    }
}
