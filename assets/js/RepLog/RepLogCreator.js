import React, {Component} from 'react'
import PropTypes from 'prop-types'
import Button from '../components/Button'

export default class RepLogCreator extends Component {
  constructor (props) {
    super(props)

    this.state = {
      quantityInputError: ''
    }

    this.handleFormSubmit = this.handleFormSubmit.bind(this)

    this.itemSelect = React.createRef()
    this.quantityInput = React.createRef()
  }

  handleFormSubmit (event) {
    event.preventDefault()

    const { onAddRepLog } = this.props
    const itemSelect = this.itemSelect.current
    const quantityInput = this.quantityInput.current

    if (quantityInput.value <= 0) {
      this.setState({ quantityInputError: 'Please enter a value greater than 0' })
      // dont' submit, or clear the form
      return
    }

    onAddRepLog(
      itemSelect.options[itemSelect.selectedIndex].value,
      parseInt(quantityInput.value)
    )

    itemSelect.selectedIndex = 0
    quantityInput.value = ''
    this.setState({ quantityInputError: '' })
  }

  render () {
    const { quantityInputError } = this.state
    const { itemOptions, validationErrorMessage } = this.props

    return (
        <form onSubmit={this.handleFormSubmit}>
            {validationErrorMessage && (
                <div className="alert alert-danger">{validationErrorMessage}</div>
            )}
            <div className="form-group">
                <label className="sr-only control-label required" htmlFor="rep_log_item">
                    What did you lift?
                </label>
                <select id="rep_log_item"
                        ref={this.itemSelect}
                        required="required"
                        className="form-control">
                    <option value="">What did you lift?</option>
                    {itemOptions.map(option =>
                        <option key={option.id} value={option.id}>{option.text}</option>
                    )}
                </select>
            </div>
            {' '}
            <div className={`form-group ${quantityInputError ? 'has-error' : ''}`}>
                <label className="sr-only control-label required" htmlFor="rep_log_reps">
                    How many times?
                </label>
                <input type="number"
                       id="rep_log_reps"
                       ref={this.quantityInput}
                       required="required"
                       placeholder="How many times?"
                       className="form-control"/>
                {quantityInputError && <span className="help-block">{quantityInputError}</span>}
            </div>
            {' '}
            <Button type="submit" className="btn-primary">
                I Lifted it! <span className="fa fa-plus-circle"/>
            </Button>
        </form>
    )
  }
}

RepLogCreator.propTypes = {
  itemOptions: PropTypes.array.isRequired,
  onAddRepLog: PropTypes.func.isRequired,
  validationErrorMessage: PropTypes.string.isRequired
}
