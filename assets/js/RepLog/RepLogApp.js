import React, { Component } from 'react'
import PropTypes from 'prop-types'
import RepLogs from './RepLogs'
import { createRepLog, deleteRepLog, getRepLogs } from '../api/rep_log_api'

export default class RepLogApp extends Component {
  constructor (props) {
    super(props)

    this.state = {
      highlightedRowId: null,
      isLoaded: false,
      isSavingNewRepLog: false,
      newRepLogValidationErrorMessage: '',
      numberOfHearts: 1,
      repLogs: [],
      successMessage: ''
    }

    this.successMessageTimeoutHandle = ''

    this.handleDeleteRepLog = this.handleDeleteRepLog.bind(this)
    this.handleHeartChange = this.handleHeartChange.bind(this)
    this.handleNewRepLog = this.handleNewRepLog.bind(this)
    this.handleRowClick = this.handleRowClick.bind(this)
  }

  componentDidMount () {
    getRepLogs().then(data => {
      this.setState({
        isLoaded: true,
        repLogs: data
      })
    })
  }

  componentWillUnmount () {
    clearTimeout(this.successMessageTimeoutHandle)
  }

  handleDeleteRepLog (id) {
    this.setState(previousState => {
      const repLogs = previousState.repLogs
        .map(repLog => {
          if (repLog.id !== id) {
            return repLog
          }
          return {
            ...repLog,
            isDeleting: true
          }
        })
      return { repLogs }
    })
    deleteRepLog(id)
      .then(() => {
        this.setState(previousState => {
          const newRepLogs = [...previousState.repLogs].filter(repLog => repLog.id !== id)
          return { repLogs: newRepLogs }
        })
        this.setSuccessMessage('Item was Un-lifted!')
      })
  }

  handleHeartChange (heartCount) {
    this.setState({ numberOfHearts: heartCount })
  }

  handleNewRepLog (item, reps) {
    const newRep = {
      item,
      reps
    }

    this.setState({
      isSavingNewRepLog: true
    })

    const newState = { isSavingNewRepLog: false }

    createRepLog(newRep)
      .then(repLog => {
        this.setState(previousState => {
          const newRepLogs = [...previousState.repLogs, repLog]
          return {
            ...newState,
            newRepLogValidationErrorMessage: '',
            repLogs: newRepLogs
          }
        })
        this.setSuccessMessage('Rep Log Saved!')
      })
      .catch(error => {
        error.response.json().then(errorsData => {
          const errors = errorsData.errors
          const firstError = errors[Object.keys(errors)[0]]
          this.setState({
            ...newState,
            newRepLogValidationErrorMessage: firstError
          })
        })
      })
  }

  handleRowClick (repLogId) {
    this.setState({ highlightedRowId: repLogId })
  }

  setSuccessMessage (message) {
    this.setState({ successMessage: message })
    clearTimeout(this.successMessageTimeoutHandle)
    this.successMessageTimeoutHandle = setTimeout(() => {
      this.setState({ successMessage: '' })
      this.successMessageTimeoutHandle = 0
    }, 3000)
  }

  render () {
    return <RepLogs
          {...this.props}
          {...this.state}
          onAddRepLog={this.handleNewRepLog}
          onDeleteRepLog={this.handleDeleteRepLog}
          onHeartChange={this.handleHeartChange}
          onRowClick={this.handleRowClick}
      />
  }
}

RepLogApp.defaultProps = {
  itemOptions: []
}
RepLogApp.propTypes = {
  itemOptions: PropTypes.array,
  withHeart: PropTypes.bool
}
