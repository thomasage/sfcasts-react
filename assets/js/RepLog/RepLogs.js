import React from 'react'
import PropTypes from 'prop-types'
import RepLogCreator from './RepLogCreator'
import RepLogList from './RepLogList'

const calculateTotalWeightLifted = repLogs => repLogs.reduce((total, repLog) => total + repLog.totalWeightLifted, 0)

export default function RepLogs ({
  highlightedRowId,
  isLoaded,
  isSavingNewRepLog,
  itemOptions,
  newRepLogValidationErrorMessage,
  numberOfHearts,
  onAddRepLog,
  onDeleteRepLog,
  onHeartChange,
  onRowClick,
  repLogs,
  successMessage,
  withHeart
}) {
  let heart = ''
  if (withHeart) {
    heart = <span>{'❤'.repeat(numberOfHearts)}</span>
  }

  return (
        <div>
            <h2>Lift History {heart}</h2>
            <input
                type="range"
                min="0"
                max="100"
                value={numberOfHearts}
                onChange={event => onHeartChange(parseInt(event.target.value))}
            />
            {successMessage && (
                <div className="alert alert-success text-center">{successMessage}</div>
            )}
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>What</th>
                    <th>How many times?</th>
                    <th>Weight</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <RepLogList
                    highlightedRowId={highlightedRowId}
                    isLoaded={isLoaded}
                    isSavingNewRepLog={isSavingNewRepLog}
                    onDeleteRepLog={onDeleteRepLog}
                    onRowClick={onRowClick}
                    repLogs={repLogs}
                />
                <tfoot>
                <tr>
                    <td>&nbsp;</td>
                    <th>Total</th>
                    <th>{calculateTotalWeightLifted(repLogs)}</th>
                    <td>&nbsp;</td>
                </tr>
                </tfoot>
            </table>
            <div className="row">
                <div className="col-md-6">
                    <RepLogCreator
                        itemOptions={itemOptions}
                        onAddRepLog={onAddRepLog}
                        validationErrorMessage={newRepLogValidationErrorMessage}
                    />
                </div>
            </div>
        </div>
  )
}

RepLogs.propTypes = {
  highlightedRowId: PropTypes.any,
  isLoaded: PropTypes.bool.isRequired,
  isSavingNewRepLog: PropTypes.bool.isRequired,
  itemOptions: PropTypes.array.isRequired,
  newRepLogValidationErrorMessage: PropTypes.string.isRequired,
  numberOfHearts: PropTypes.number.isRequired,
  onAddRepLog: PropTypes.func.isRequired,
  onDeleteRepLog: PropTypes.func.isRequired,
  onHeartChange: PropTypes.func.isRequired,
  onRowClick: PropTypes.func.isRequired,
  repLogs: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    itemLabel: PropTypes.string.isRequired,
    reps: PropTypes.number.isRequired,
    totalWeightLifted: PropTypes.number.isRequired
  })),
  successMessage: PropTypes.string.isRequired,
  withHeart: PropTypes.bool
}
