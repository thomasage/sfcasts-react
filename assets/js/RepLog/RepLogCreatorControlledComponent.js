import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class RepLogCreatorControlledComponent extends Component {
  constructor (props) {
    super(props)

    this.state = {
      quantityInputError: '',
      quantityValue: 0,
      selectedItemId: ''
    }

    this.handleFormSubmit = this.handleFormSubmit.bind(this)
    this.handleQuantityInputChange = this.handleQuantityInputChange.bind(this)
    this.handleSelectedItemChange = this.handleSelectedItemChange.bind(this)

    this.itemOptions = [
      { id: 'cat', text: 'Cat' },
      { id: 'fat_cat', text: 'Big Fat Cat' },
      { id: 'laptop', text: 'My Laptop' },
      { id: 'coffee_cup', text: 'Coffee Cup' }
    ]
  }

  handleFormSubmit (event) {
    event.preventDefault()

    const { onAddRepLog } = this.props
    const { quantityValue, selectedItemId } = this.state

    const itemLabel = this.itemOptions.find(option => option.id === selectedItemId).text

    if (quantityValue <= 0) {
      this.setState({ quantityInputError: 'Please enter a value greater than 0' })
      // dont' submit, or clear the form
      return
    }

    onAddRepLog(itemLabel, parseInt(quantityValue))

    this.setState({
      quantityInputError: '',
      quantityValue: 0,
      selectedItemId: ''
    })
  }

  handleQuantityInputChange (event) {
    this.setState({ quantityValue: event.target.value })
  }

  handleSelectedItemChange (event) {
    this.setState({ selectedItemId: event.target.value })
  }

  render () {
    const { quantityInputError, quantityValue, selectedItemId } = this.state

    return (
        <form onSubmit={this.handleFormSubmit}>
            <div className="form-group">
                <label className="sr-only control-label required" htmlFor="rep_log_item">
                    What did you lift?
                </label>
                <select id="rep_log_item"
                        ref={this.itemSelect}
                        required="required"
                        className="form-control"
                        value={selectedItemId}
                        onChange={this.handleSelectedItemChange}>
                    <option value="">What did you lift?</option>
                    {this.itemOptions.map(option =>
                        <option key={option.id} value={option.id}>{option.text}</option>
                    )}
                </select>
            </div>
            {' '}
            <div className={`form-group ${quantityInputError ? 'has-error' : ''}`}>
                <label className="sr-only control-label required" htmlFor="rep_log_reps">
                    How many times?
                </label>
                <input type="number"
                       id="rep_log_reps"
                       value={quantityValue}
                       required="required"
                       placeholder="How many times?"
                       className="form-control"
                       onChange={this.handleQuantityInputChange}/>
                {quantityInputError && <span className="help-block">{quantityInputError}</span>}
            </div>
            {' '}
            <button type="submit" className="btn btn-primary">I Lifted it!</button>
        </form>
    )
  }
}

RepLogCreatorControlledComponent.propTypes = {
  onAddRepLog: PropTypes.func.isRequired
}
