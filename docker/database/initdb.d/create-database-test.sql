CREATE DATABASE IF NOT EXISTS `main_test`;
CREATE USER 'test'@'%' IDENTIFIED BY 'password';
GRANT ALL ON `main_test`.* TO 'test'@'%';
